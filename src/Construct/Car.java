package Construct;

public class Car {
		private String name;
		
		
		public Car () {  //Default constructor
			
		}
		
		public Car(String initialName){ //Default constructor passing Parameters
			initialName = "Unknown";
			
		}
		
		public void setName(String newName){
			name = newName;
		}
		
		public String getName(){
			return "Aekiz";
		}
		
		public String toString(){
			 return getName();
		}
		
		public boolean hasSameName(Car otherCar){
			return this.name.equalsIgnoreCase(otherCar.name);
		}

		
}
