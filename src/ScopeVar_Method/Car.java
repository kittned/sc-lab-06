package ScopeVar_Method;

public class Car {
	private double c1; // variable with the same name
	private String numCar = "1234CA"; 
	
	public void setNumCar(){
	        String numCar;  // Local variable
	        numCar = "AC4321";  // Local variable
	    }
	 
	//Scope of Local Variables
		public static void main(String[] args){
			Car c1 = new Car();
	        c1.setNumCar();
	        System.out.println(c1.numCar);
	}
}


