package Abstaction;

import java.util.ArrayList;
public class Main {
	
	public static void main(String[] args){
		Motorcycle motor = new Motorcycle("Honda Wave");
		Car cars = new Car ("Mitsubishi");
		Truck trucks = new Truck("Toyota");
		System.out.println(motor);
		System.out.println(cars);
		System.out.println(trucks);
		
		ArrayList<Vechicle> vech = new ArrayList<Vechicle>();
		vech.add(motor);
		vech.add(cars);
		vech.add(trucks);
		
		for (Vechicle vechicle : vech){
			System.out.println(vechicle.numCar());
		}
	}

}
